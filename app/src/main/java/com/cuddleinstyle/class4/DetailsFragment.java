package com.cuddleinstyle.class4;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class DetailsFragment extends Fragment {

    Stock s;

    public DetailsFragment() {
        // Required empty public constructor
    }
    TextView tv;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.details_fragment, container, false);
        tv = (TextView)v.findViewById(R.id.fragmenttv);
        Bundle args = getArguments();
        if (args != null) {
            Stock s = (Stock)args.getSerializable("stock");
            tv.setText(s.name);
        }
        return v;
    }

    public void setStock(Stock s) {
        this.s = s;
        tv.setText(s.name);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }
}
