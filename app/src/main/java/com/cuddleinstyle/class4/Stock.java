package com.cuddleinstyle.class4;

import java.io.Serializable;

/**
 * Created by anmol on 5/9/15.
 */
public class Stock implements Serializable{
    CharSequence handle;
    CharSequence name;
    CharSequence value;

    Stock(CharSequence handle,CharSequence name, CharSequence value){
        this.handle = handle;
        this.name = name;
        this.value = value;
    }

    Stock(){
        this.name = "";
        this.handle = "";
        this.value = "0";
    }


}
