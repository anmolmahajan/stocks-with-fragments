package com.cuddleinstyle.class4;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class ListFragment extends Fragment implements StockInterface{

    ListView lv;
    ArrayList<Stock> data;
    ArrayList<String> ticker;
    CustomAdapter adapter;
    ProgressDialog pd;
    Context app;


    public interface StockClicked{
        void onStockClicked(Stock item);
    }

    public ListFragment() {
        // Required empty public constructor
    }

    public void getdata(){
        String urlString = getURLString();
        StocksFetcherAsync sfa = new StocksFetcherAsync();
        sfa.listener = this;
        Log.i("urlstring", urlString);
        sfa.execute(urlString);
        pd = new ProgressDialog(getActivity());
        pd.setTitle("Fetching");
        pd.setMessage("Fetching the data. Please wait.");
        pd.show();
    }

    public String getURLString(){
        String s = "http://query.yahooapis.com/v1/public/yql?q=select%20*%20" +
                "from%20yahoo.finance.quotes%20where%20symbol%20in%20(%22";


        if(ticker == null || ticker.size() == 0) {
            ticker.add("MSFT");
        }
        for (int i = 0; i < ticker.size() - 1; i++) {
            s += ticker.get(i) + "%22%2C%22";
        }
        s += ticker.get(ticker.size() - 1);


        s += "%22)%0A%09%09&env=http%3A%2F%2Fdatatables.org%2Falltables.env&format=json";
        Log.i("output", s);
        return s;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){

        data = new ArrayList<>();
        ticker = new ArrayList<>();
        getbackupdata();
        getdata();
        View v = inflater.inflate(R.layout.fragment_layout,container,false);
        lv = (ListView) v.findViewById(R.id.lv);
        app = getActivity();                             //For toast context
        adapter = new CustomAdapter(getActivity(),data);        //For listview
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Stock item = adapter.getItem(position);
                StockClicked a = (StockClicked)getActivity();
                a.onStockClicked(item);
            }
        });
        setHasOptionsMenu(true);
        return v;
    }

    public void StocksTaskOnComplete(Stock[] stock) {
        if(stock != null){
            data.clear();
            for (Stock s : stock) {
                data.add(s);
            }
        }
        adapter.notifyDataSetChanged();
        pd.dismiss();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if(id == R.id.edit){
            Intent i = new Intent();
            i.setClass(getActivity(),Suggestions.class);
            startActivityForResult(i, 1);
        }
        else if(id == R.id.refresh){
            getdata();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setHasOptionsMenu(boolean hasMenu) {
        super.setHasOptionsMenu(true);
    }

    @Override
    public void onStop() {
        try {
            FileOutputStream output = getActivity().openFileOutput("tickers.txt", Context.MODE_PRIVATE);
            DataOutputStream dout = new DataOutputStream(output);
            dout.writeInt(ticker.size()); // Save line count
            for(int i = 0; i < ticker.size(); i++){
                dout.writeUTF(ticker.get(i));
            }
            dout.flush(); // Flush stream ...
            dout.close(); // ... and close.
        }catch (IOException e){ e.printStackTrace(); };
        super.onStop();
        super.onStop();
    }

    public void getbackupdata(){
        try {
            FileInputStream input = getActivity().openFileInput("tickers.txt"); // Open input stream
            DataInputStream din = new DataInputStream(input);
            int sz = din.readInt(); // Read line count
            for (int i = 0; i < sz; i++) { // Read lines
                ticker.add(din.readUTF());
            }
            din.close();
        }catch (IOException e){ e.printStackTrace(); }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.menu_main, menu);
    }



    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 1){
            if(resultCode == 1){
                Bundle b = data.getExtras();
                String s = b.getString("names");
                addnames(s);
                getdata();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    void addnames(String s){
        String k = "";
        for(int i = 0; i < s.length(); i++){

            if(s.charAt(i) == '^'){
                ticker.add(k);
                k = "";
            }else{
                k += s.charAt(i);
            }
        }
    }




}
