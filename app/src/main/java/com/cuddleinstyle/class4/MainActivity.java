package com.cuddleinstyle.class4;

import android.app.Fragment;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.FrameLayout;

public class MainActivity extends AppCompatActivity implements ListFragment.StockClicked{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void onStockClicked(Stock item) {
        FrameLayout container = (FrameLayout) findViewById(R.id.container);
        if(container == null){
            Intent i = new Intent();
            i.setClass(this,DetailsActivity.class);
            i.putExtra("Stock", item);
            startActivity(i);
        }else{
            DetailsFragment df = new DetailsFragment();
            Bundle b = new Bundle();
            b.putSerializable("stock", item);
            df.setArguments(b);
            getSupportFragmentManager().beginTransaction().replace(R.id.container,df).commit();
        }
    }

}
